#!/bin/bash
set -euo pipefail

echo '
    ____  ________  ____  ____ _________
   / __ \/ ___/ _ \/ __ \/ __ `/ ___/ _ \
  / /_/ / /  /  __/ /_/ / /_/ / /  /  __/
 / .___/_/   \___/ .___/\__,_/_/   \___/
/_/             /_/
'

# Allow running the tests locally
if [ ! -v CI_PROJECT_DIR ]; then
    export CI_PROJECT_DIR=$(pwd)
    export CI_PIPELINE_ID=12345
fi
# Add some example projects to test.
export GITHUB_PROJECTS="skt kpet"
export GITLAB_COM_PROJECTS="cki-lib datadefinition kernel-configs"
export GITLAB_CEE_PROJECTS=""
# Additional variables normally supplied via the pipeline.
export REPO_DIR=${CI_PROJECT_DIR}/software/repos
export WHEELHOUSE_DIR=${CI_PROJECT_DIR}/software/wheels
export PIPELINE_DEFINITION_URL=https://gitlab.com/cki-project/pipeline-definition.git
export PIPELINE_DEFINITION_DIR=${CI_PROJECT_DIR}

SOFTWARE_OBJECT=pipeline-${CI_PIPELINE_ID}.tar.xz

# Verify the prepare stage.
function verify_prepare {
  source scripts/functions.sh

  echo_green "Checking for object in minio."
  aws_s3_ls BUCKET_SOFTWARE "${SOFTWARE_OBJECT}"

  echo_green "Downloading object and extracting it."
  aws_s3_download BUCKET_SOFTWARE "${SOFTWARE_OBJECT}" .
  mkdir /tmp/software_extracted
  tar -xf ${SOFTWARE_OBJECT} -C /tmp/software_extracted

  pushd /tmp/software_extracted
    echo_green "Verifying directories are present."
    test -d software
    test -d software/repos
    test -d software/wheels

    echo_green "Verifying project repos."
    GITHUB_PROJECTS_ARRAY=($GITHUB_PROJECTS)
    for PROJECT in "${GITHUB_PROJECTS_ARRAY[@]}"; do
        test -f software/repos/${PROJECT}.tar
    done
    GITLAB_COM_PROJECTS_ARRAY=($GITLAB_COM_PROJECTS)
    for PROJECT in "${GITLAB_COM_PROJECTS_ARRAY[@]}"; do
        test -f software/repos/${PROJECT}.tar
    done

    echo_green "Verifying wheels."
    virtualenv --python=/usr/bin/python3 verify-wheels --no-download
    verify-wheels/bin/python3 -m pip install --quiet --no-index \
      --find-links software/wheels \
      software/wheels/*.whl
  popd

  rm -rf /tmp/software_extracted ${SOFTWARE_OBJECT}

  echo_green "🥳🤩 SUCCESS!"
}

eval "$(tests/setup-minio.sh)"

# Run the prepare stage
source scripts/prepare_software.sh
verify_prepare

# Run the prepare stage when the software is already present.
source scripts/prepare_software.sh
verify_prepare

# Run the prepare stage when the software is packaged in object storage
# but not present on the host.
rm -rf software
source scripts/prepare_software.sh
verify_prepare

# Run the prepare stage when the software is packaged in object storage
# and an update is forced
echo invalid-tar-file > /tmp/invalid.txt
tar -cf - /tmp/invalid.txt | xz -z - > /tmp/invalid.tar.xz
aws --endpoint-url "${LOCALMINIO_ENDPOINT}" s3 cp /tmp/invalid.tar.xz "s3://${SOFTWARE_BUCKET}/${SOFTWARE_PATH}${SOFTWARE_OBJECT}"
rm -rf software/wheels
FORCE_SOFTWARE_PREPARE=true
source scripts/prepare_software.sh
verify_prepare

# Cleanup
rm -rf wheel-verify-venv software
